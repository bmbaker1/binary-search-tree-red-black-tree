/* Brandon Baker
 * CS 201
 * Assignment #2
 * bst.c
 */

#include "bst.h"
#include <stdlib.h>
#include <stdarg.h>
#include "queue.h"

#define MIN 0
#define MAX 1

int rowfinished = 0;


/* Private Method Headers */

static void fatalBST(char *message, ...);
static void addChildrenToQueue(queue *q, bstNode *node);
static void swapQueues(queue *q1, queue *q2);

static bstNode *newBSTNode(void *value);
static int isEmptyBST(bst *tree);
static bstNode *insertBSTUtil(bst *tree, bstNode *new, bstNode *currnode);
static bstNode *findBSTNodeUtil(bst *tree, void *key, bstNode *curr);
static void printRowNumber(FILE *fp, int rowcount);
static void printIfLeaf(FILE *fp, bstNode *node);
static void printPosition(FILE *fp, bstNode *node);
static void printInfoBlock(FILE *fp, bst *tree, int rowcount, bstNode *node);
static void printBSTUtil(FILE *fp, bst *tree);
static int getHeight(bstNode *curr, int minormax);
static int nodeIsLeaf(bstNode *node);
static int hasLeftNode(bstNode *node);
static void swapNodeValues(bstNode *this, bstNode *that);


/* Private Method Implementations */

void fatalBST(char *message, ...) {
	va_list ap;

	fprintf(stderr, "Error: ");
	va_start(ap, message);
	vfprintf(stderr, message, ap);
	vfprintf(stderr, "... Exiting\n", ap);
	va_end(ap);

	exit(666);
}

void addChildrenToQueue(queue *q, bstNode *node) {
	enqueue(q, node->left);
	enqueue(q, node->right);
}

void swapQueues(queue *this, queue *that) {
	queue temp = *this;
	*this = *that;
	*that = temp;
}

int isEmptyBST(bst *tree) {
	return tree->root == NULL;
}

bstNode *insertBSTUtil(bst *tree, bstNode *new, bstNode *currnode) {
	new->parent = currnode;

	if(tree->compare(new->value, currnode->value) < 0) {
		if(!currnode->left)
			currnode->left  = new;
		else
			insertBSTUtil(tree, new, currnode->left);
	} else if(tree->compare(new->value, currnode->value) > 0) {
		if(!currnode->right)
			currnode->right  = new;
		else
			insertBSTUtil(tree, new, currnode->right);
	}
	return new;
}

bstNode *findBSTNodeUtil(bst *tree, void *key, bstNode *curr) {
	if(!isEmptyBST(tree)){
		int comparison = tree->compare(key, curr->value);

		if(comparison == 0) return curr;
		else if(comparison < 0) {
			if(!curr->left) return NULL;
			else return findBSTNodeUtil(tree, key, curr->left);
		} else {
			if(!curr->right) return NULL;
			else return findBSTNodeUtil(tree, key, curr->right);
		}
	}
	return curr;
}

int isLeftChild(bstNode *node) {
	return(node->parent->left == node);
}

void printRowNumber(FILE *fp, int rowcount) {
	if(!rowfinished) {
		fprintf(fp, "%d:", rowcount);
		rowfinished = 1;
	}
}

void printIfLeaf(FILE *fp, bstNode *node) {
	if(node->right == NULL && node->left == NULL) fprintf(fp, "=");
}

void printPosition(FILE *fp, bstNode *node) {
	fprintf(fp, "-");

	if(node->parent == node) return;

	isLeftChild(node) ? fprintf(fp, "l") : fprintf(fp, "r");
}

void printInfoBlock(FILE *fp, bst *tree, int rowcount, bstNode *node) {
	printRowNumber(fp, rowcount);

	fprintf(fp, " ");
	printIfLeaf(fp, node);
	tree->display(fp, node->value);

	fprintf(fp, "(");
	tree->display(fp, node->parent->value);
	fprintf(fp, ")");

	printPosition(fp, node);
}

void printBSTUtil(FILE *fp, bst *tree) {
	int rowcount = 0;
	queue *currlevel = newQueue(tree->display),
		  *nextlevel = newQueue(tree->display);
	bstNode  *node = tree->root;

	enqueue(currlevel, node);

	while(sizeQueue(currlevel) != 0) {
		node = dequeue(currlevel);

		if(node) {
			printInfoBlock(fp, tree, rowcount, node);
			addChildrenToQueue(nextlevel, node);
		}
		if(sizeQueue(currlevel) == 0) {
			if(sizeQueue(nextlevel) != 0) fprintf(fp, "\n");

			rowcount++;
			rowfinished = 0;

			swapQueues(currlevel, nextlevel);
		}
	}
}

int getHeight(bstNode *curr, int minormax) {
	if(!curr) return 0;

	int leftheight = getHeight(curr->left, minormax);
	int rightheight = getHeight(curr->right, minormax);

	if(minormax == MIN)
		return (leftheight > rightheight) ? rightheight + 1 : leftheight + 1;
	else
		return (leftheight > rightheight) ? leftheight + 1 : rightheight + 1;
}

void swapNodeValues(bstNode *this, bstNode *that) {
	void  *temp = this->value;
	this->value = that->value;
	that->value = temp;
}

int nodeIsLeaf(bstNode *node) {
	return (node->left == NULL && node->right == NULL);
}

int hasLeftNode(bstNode *node) {
	return (node->left != NULL);
}


/* Public Method Implementations */

bstNode *newBSTNode(void *value) {
	bstNode *newnode;
	if((newnode = malloc(sizeof(bstNode))) == 0)
		fatalBST("BSTNode not allocated\n");

	newnode->value = value;
	newnode->parent = newnode->left = newnode->right = NULL;

	return newnode;
}

bst *newBST(void (*display)(FILE *, void *),
									int (*comparator)(void *, void *)) {
	bst *newbst;
	if((newbst = malloc(sizeof(bst))) == 0)
		fatalBST("BST not allocated\n");

	newbst->root = NULL;
	newbst->size = 0;
	newbst->display = display;
	newbst->compare = comparator;

	return newbst;
}

bstNode *insertBST(bst *tree, void *value) {
	bstNode *newnode = newBSTNode(value);

	if(isEmptyBST(tree)) newnode->parent = tree->root = newnode;
	else insertBSTUtil(tree, newnode, tree->root);

	tree->size++;

	return newnode;
}

int findBST(bst *tree, void *value) {
	return findBSTNodeUtil(tree, value, tree->root) != NULL;
}

bstNode *findBSTNode(bst *tree, void *value) {
	bstNode *foundnode;

	if(isEmptyBST(tree)) foundnode = NULL;
	else foundnode = findBSTNodeUtil(tree, value, tree->root);

	return foundnode;
}

bstNode *swapToLeafBSTNode(bstNode *node) {
	if(nodeIsLeaf(node)) return node;

	bstNode *child;

	if(hasLeftNode(node)) {
		child = node->left;

		while(child->right) child = child->right;
	} else {
		child = node->right;

		while(child->left) child = child->left;
	}

	swapNodeValues(node, child);

	return swapToLeafBSTNode(child);
}

void pruneBSTNode(bst *tree, bstNode *node) {
	if(node == tree->root) tree->root = NULL;

	if(isLeftChild(node)) node->parent->left = NULL;
	else node->parent->right = NULL;

	tree->size--;
}

int sizeBST(bst *tree) {
	return tree->size;
}

void statisticsBST(bst *tree, FILE *fp) {
	fprintf(fp, "Nodes: %d\n", sizeBST(tree));
	fprintf(fp, "Minimum depth: %d\n", getHeight(tree->root, MIN));
	fprintf(fp, "Maximum depth: %d\n", getHeight(tree->root, MAX));
}

void displayBST(FILE *fp, bst *tree) {
	if(isEmptyBST(tree)) fprintf(fp, "0:\n");
	else printBSTUtil(fp, tree);
}
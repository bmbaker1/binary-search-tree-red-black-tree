/* Brandon Baker
 * CS 201
 * Assignment #2
 * bst.h
 * Public interface for bst.c, which is a set of generic functions to create
 * and maintain a binary search tree
 */

#ifndef ASS2_BST_H
#define ASS2_BST_H

#include <stdio.h>

typedef struct bstNode {
	void *value;
	struct bstNode *parent, *left, *right;
} bstNode;

typedef struct bst {
	bstNode *root;
	int size;
	void (*display)(FILE *, void *);
	int (*compare)(void *, void *);
} bst;


/*
 * function:   	newBST
 * parameter/s:	display and compare function for generic data type
 * description:	creates a new generic bst object
 * returns:    	the created bst object
 */
extern bst *newBST(void (*display)(FILE *, void *),
				  						int (*comparator)(void *, void *));

/*
 * function:    insertBST
 * parameter/s:	a bst object and value to add to the bst
 * description: adds a value to the binary tree if it is unique
 * returns:		the node that was inserted into the tree
 */
extern bstNode *insertBST(bst *tree, void *value);

/*
 * function: 	findBST
 * parameter/s:	a bst object and a value to search for
 * description: searches for the value passed in to see if it exists
 * returns:		1 if  the value exists in the tree, 0 otherwise
 */
extern int findBST(bst *tree, void *value);

/*
 * function:	findBSTNode
 * parameter/s: a bst object and a value to search for
 * description: searches for the value passed in
 * returns:		if value exists in tree, returns the node.  NULL otherwise
 */
extern bstNode *findBSTNode(bst *tree, void *value);

/*
 * function:	swapToLeafBSTNode
 * parameter/s:	a bst node
 * description: swaps given node down binary tree until it is a leaf
 * 				while maintaining binary tree ordering
 * returns:		the node that was moved to a leaf
 */
extern bstNode *swapToLeafBSTNode(bstNode *node);

/*
 * function:	pruneBSTNode
 * parameter/s: a bst object and the node to be pruned
 * description:	cuts all pointers to the given bst node
 * returns:		void
 */
extern void pruneBSTNode(bst *, bstNode *node);

/*
 * function:	sizeBST
 * parameter/s: a bst object
 * description: gets the size of a give binary tree
 * returns: 	the binary search tree's size
 */
extern int sizeBST(bst *tree);

/*
 * function:	statisticsBST
 * parameter/s: a bst object and an ouput file
 * description: displays the statistics of a tree, include node amount,
 * 				minimum height, and maximum height
 * returns:		void
 */
extern void statisticsBST(bst *tree, FILE *fp);

/*
 * function:	displayBST
 * parameter/s: output file and a bst object
 * description: displays the binary tree in level order
 * returns:		void
 */
extern void displayBST(FILE *fp, bst *tree);

#endif //ASS2_BST_H

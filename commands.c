/* Brandon Baker
 * CS 201
 * Assignment #2
 * commands.c
 */

#include "commands.h"
#include "utils.h"
#include "tokenutils.h"
#include "vbst.h"
#include "rbt.h"
#include "string.h"

#define VBST 'v'
#define INSERT 'i'
#define DELETE 'd'
#define FREQ 'f'
#define SHOW 's'
#define REPORT 'r'


/* Private Method Prototypes */

static void commandInsert(char treetype, void *tree, void *value);
static void commandDelete(char treetype, void *tree, void *value);
static void commandFindFrequency(char treetype, void *tree,
								 void *vlaue, FILE *out);
static void commandDisplayTree(char treetype, void *tree, FILE *out);
static void commandStatistics(char treetype, void *tree, FILE *out);
static void printWordFrequency(char treetype, void *tree, void *value,
							   int freq, FILE *out);


/* Private Method Implementations */

void commandInsert(char treetype, void *tree, void *value) {
	if(!isEmptyToken(value)) {
		if (treetype == VBST) insertVBST(tree, newString(value));
		else insertRBT(tree, newString(value));
	}
}

void commandDelete(char treetype, void *tree, void *value) {
	if(treetype == VBST) deleteVBST(tree, newString(value));
	else deleteRBT(tree, newString(value));
}

void commandFindFrequency(char treetype, void *tree,
						  void *value, FILE *out) {
	int freq;

	if(treetype == VBST) freq = findVBST(tree, newString(value));
	else freq = findRBT(tree, newString(value));

	printWordFrequency(treetype, tree, value, freq, out);
}

void commandDisplayTree(char treetype, void *tree, FILE *out) {
	if(treetype == VBST) displayVBST(out, tree);
	else displayRBT(out, tree);
}

void commandStatistics(char treetype, void *tree, FILE *out) {
	if(treetype == VBST) statisticsVBST(tree, out);
	else statisticsRBT(tree, out);
}

void printWordFrequency(char treetype, void *tree, void *value,
						int freq, FILE *out) {
	fprintf(out, "Frequency of ");

	if(treetype == VBST) {
		vbst *vbstree = tree;
		vbstree->display(out, newString(value));
	} else {
		rbt *rbtree = tree;
		rbtree->display(out, newString(value));
	}

	fprintf(out, ": %d\n", freq);
}


/* Public Interface Implementations */

void interpretCommands(char treetype, void *tree,
					   FILE *commands, FILE *out) {
	char command;

	while(!feof(commands)) {
		command = getNextCommand(commands);

		if(command == INSERT)
			commandInsert(treetype, tree, getNextToken(commands));
		else if(command == DELETE)
			commandDelete(treetype, tree, getNextToken(commands));
		else if(command == FREQ)
			commandFindFrequency(treetype, tree, getNextToken(commands), out);
		else if(command == SHOW)
			commandDisplayTree(treetype, tree, out);
		else if(command == REPORT)
			commandStatistics(treetype, tree, out);
	}
}

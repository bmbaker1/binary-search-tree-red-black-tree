/* Brandon Baker
 * CS 201
 * Assignment #2
 * commands.h
 * Public interface for comands.h, which just starts the process of reading
 * in a command file and processing what that file says
 */
#ifndef ASS2_COMMANDS_H
#define ASS2_COMMANDS_H

#include <stdio.h>

/*
 * function:	interpretCommands
 * parameter/s:	type of tree to run commands on, a binary tree, the command
 * 				file, and the output file
 * description: reads from file and runs commands based on what type of tree
 * 				type is passed in
 * returns: 	void
 */
extern void interpretCommands(char treetype, void *tree,
							  FILE *commands, FILE *out);

#endif //ASS2_COMMANDS_H

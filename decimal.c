/* Brandon Baker
 * CS 201
 * Assignment #1
 * decimal.c
 */

#include "decimal.h"
#include <stdlib.h>
#include "utils.h"

/* Public Interface Implementations */

decimal *newDecimal(int v) {
	decimal *newdecimal;
	if((newdecimal = malloc(sizeof(decimal))) == 0)
		fatal("Decimal not allocated... Exiting\n");

	newdecimal->value = v;

	return newdecimal;
}

int getDecimal(decimal *this) {
	return this->value;
}

int compareDecimal(void *this, void *that) {
	return(getDecimal(this) - getDecimal(that));
}

void displayDecimal(FILE *out, void *value) {
	fprintf(out, "%d", getDecimal(value));
}


/* Brandon Baker
 * CS 201
 * Assignment #1
 * decimal.h
 * [description]
 */
#ifndef ASS1_DECIMAL_H
#define ASS1_DECIMAL_H

#include <stdio.h>

typedef struct decimal {
	int value;
} decimal;

extern decimal *newDecimal(int);
extern int getDecimal(decimal *);
extern int compareDecimal(void *, void *);
extern void displayDecimal(FILE *, void *);

#endif //ASS1_DECIMAL_H

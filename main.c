/* Brandon Baker
 * CS 201
 * Assignment #2
 * main.c
 */

#include <stdio.h>
#include "utils.h"
#include "commands.h"
#include "tree.h"

int main(int argc, char **argv) {
	if(argc < 4)
		fatal("Incorrect amount of arguments.  Please run program with "
					  "the following arguments:\n-[type of tree] "
					  "[input file] [command file] [optional output file]");

	char treetype = getFlag(argv);
	FILE *corpus = getCorpus(argv), *commands = getCommandFile(argv);
	FILE *output = getOutputFile(argv);

	void *newtree = plantTree(treetype);
	growTree(treetype, newtree, corpus);

	interpretCommands(treetype, newtree, commands, output);

	return 0;
}

objects = sll.o queue.o utils.o decimal.o real.o string.o scanner.o tokenutils.o bst.o vbst.o rbt.o commands.o tree.o main.o

oopts = -Wall -Wextra -std=c99 -g -c
lopts = -Wall -Wextra -std=c99 -g

all: bstrees

bstrees: $(objects)
	gcc $(lopts) -o bstrees $(objects)

sll.o : sll.c sll.h
	gcc $(oopts) sll.c

queue.o : queue.c queue.h sll.h
	gcc $(oopts) queue.c

utils.o : utils.c utils.h
	gcc $(oopts) utils.c

decimal.o : decimal.c decimal.h utils.h
	gcc $(oopts) decimal.c

real.o : real.c real.h utils.h
	gcc $(oopts) real.c

string.o : string.c string.h utils.h
	gcc $(oopts) string.c

scanner.o : scanner.c scanner.h
	gcc $(oopts) scanner.c

tokenutils.o: tokenutils.c tokenutils.h
	gcc $(oopts) tokenutils.c

bst.o: bst.c bst.h
	gcc $(oopts) bst.c

vbst.o: vbst.c vbst.h
	gcc $(oopts) vbst.c

rbt.o: rbt.c rbt.h
	gcc $(oopts) rbt.c

commands.o: commands.c commands.h
	gcc $(oopts) commands.c

tree.o: tree.c tree.h
	gcc $(oopts) tree.c

main.o: main.c utils.h tree.h commands.h 
	gcc $(oopts) main.c

clean:
	rm -f $(objects) bstrees
	rm -f myOutput1.txt
	rm -f myOutput2.txt
	rm -f myOutput3.txt
	rm -f myOutput4.txt
	rm -f myOutput5.txt
	rm -f myOutput6.txt
	rm -f myOutput7.txt
	rm -f myOutput8.txt
	rm -f myOutput9.txt
	rm -f myOutput10.txt

test:
	
	./bstrees -v assign2testing/sampleCorpus1.txt assign2testing/sampleCommands1.txt assign2testing/myOutput1.txt
	./bstrees -v assign2testing/sampleCorpus2.txt assign2testing/sampleCommands2.txt assign2testing/myOutput2.txt
	./bstrees -v assign2testing/sampleCorpus3.txt assign2testing/sampleCommands3.txt assign2testing/myOutput3.txt
	./bstrees -v assign2testing/sampleCorpus4.txt assign2testing/sampleCommands4.txt assign2testing/myOutput4.txt
	./bstrees -v assign2testing/sampleCorpus4.txt assign2testing/sampleCommands5.txt assign2testing/myOutput9.txt
	./bstrees -r assign2testing/sampleCorpus1.txt assign2testing/sampleCommands1.txt assign2testing/myOutput5.txt
	./bstrees -r assign2testing/sampleCorpus2.txt assign2testing/sampleCommands2.txt assign2testing/myOutput6.txt
	./bstrees -r assign2testing/sampleCorpus3.txt assign2testing/sampleCommands3.txt assign2testing/myOutput7.txt
	./bstrees -r assign2testing/sampleCorpus4.txt assign2testing/sampleCommands4.txt assign2testing/myOutput8.txt
	./bstrees -v assign2testing/corpus0.txt assign2testing/commands0.txt assign2testing/myExtraOutput1.txt
	./bstrees -r assign2testing/corpus0.txt assign2testing/commands0.txt assign2testing/myExtraOutput2.txt
	./bstrees -v assign2testing/corpus1.txt assign2testing/commands1.txt assign2testing/myExtraOutput3.txt
	./bstrees -r assign2testing/corpus1.txt assign2testing/commands2.txt assign2testing/myExtraOutput4.txt
	./bstrees -v assign2testing/corpus2.txt assign2testing/commands3.txt assign2testing/myExtraOutput5.txt
	./bstrees -r assign2testing/corpus2.txt assign2testing/commands4.txt assign2testing/myExtraOutput6.txt
	./bstrees -v assign2testing/corpus3.txt assign2testing/commands5.txt assign2testing/myExtraOutput7.txt
	./bstrees -r assign2testing/corpus3.txt assign2testing/commands5.txt assign2testing/myExtraOutput8.txt
	diff assign2testing/myOutput1.txt assign2testing/out1.txt
	diff assign2testing/myOutput3.txt assign2testing/out3.txt
	diff assign2testing/myOutput4.txt assign2testing/out4.txt
	diff assign2testing/myOutput5.txt assign2testing/out5.txt
	diff assign2testing/myOutput6.txt assign2testing/out6.txt
	diff assign2testing/myOutput8.txt assign2testing/out8.txt
	diff assign2testing/myExtraOutput1.txt assign2testing/output1.txt
	diff assign2testing/myExtraOutput2.txt assign2testing/output2.txt
	diff assign2testing/myExtraOutput3.txt assign2testing/output3.txt
	diff assign2testing/myExtraOutput4.txt assign2testing/output4.txt
	diff assign2testing/myExtraOutput5.txt assign2testing/output5.txt
	diff assign2testing/myExtraOutput6.txt assign2testing/output6.txt
	diff assign2testing/myExtraOutput7.txt assign2testing/output7.txt
	diff assign2testing/myExtraOutput8.txt assign2testing/output8.txt

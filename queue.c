/* Brandon Baker
 * CS 201
 * Assignment #2
 * queue.c
 */

#include "queue.h"
#include <stdlib.h>
#include <stdarg.h>
#include "sll.h"

/* Private Methods */

void fatalQueue(char *message, ...) {
	va_list ap;

	fprintf(stderr, "Error: ");
	va_start(ap, message);
	vfprintf(stderr, message, ap);
	va_end(ap);

	exit(666);
}


/* Public Interface Implementations */

queue *newQueue(void (*d)(FILE *, void *)) {
	queue *newq;
	if((newq = malloc(sizeof(queue))) == 0)
		fatalQueue("Queue not allocated... Exiting\n");

	newq->list = newSLL(d);

	return newq;
}

void enqueue(queue *q, void *value) {
	insertSLL(q->list, sizeSLL(q->list), value);
}

void *dequeue(queue *q) {
	return removeSLL(q->list, 0);
}

void *peekQueue(queue *q) {
	return getSLL(q->list, 0);
}


int sizeQueue(queue *q) {
	return sizeSLL(q->list);
}

void displayQueue(FILE *file, queue *q) {
	displaySLL(file, q->list);
}

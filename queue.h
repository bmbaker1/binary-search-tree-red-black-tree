/* Brandon Baker
 * CS 201
 * Assignment #1
 * queue.h
 * User interface for a module that creates and maintains a queue data structure
 * Uses the sll module.  Enqueues to the back of the queue.  Dequeues from the
 * front of the queue.
 */

#ifndef ASS1_QUEUE_H
#define ASS1_QUEUE_H

#include <stdio.h>

typedef struct queue {
	struct sll *list;
} queue;

/*
 * function:    newQueue
 * parameter/s: displayer function for generic data type
 * description: allocates a queue
 * returns:     an empty queue
 */
extern queue *newQueue(void (*d)(FILE *, void *));

/*
 * function:   	enqueue
 * parameter/s: queue, generic value
 * description: inserts generic value onto back of the queue
 * returns:     void
 */
extern void enqueue(queue *q, void *value);

/*
 * function:    dequeue
 * parameter/s: queue
 * description: removes the element from the front of the queue
 * returns:     generic value of removed element
 */
extern void *dequeue(queue *q);

/*
 * function:    peekQueue
 * parameter/s: queue
 * description: shows the value of the element that is next to be dequeued
 * returns:     generic value of element that is next to be dequeued
 */
extern void *peekQueue(queue *q);

/*
 * function:    sizeQueue
 * parameter/s: queue
 * description: returns the size of the specified queue
 * returns:     size of the specified queue
 */
extern int sizeQueue(queue *q);

/*
 * function:    displayQueue
 * parameter/s: output locations, queue
 * description: prints specified queue in the form of [n0, n1, n2, ..., nk]
 * 				to the specified output location
 * returns:     void
 */
extern void displayQueue(FILE *file, queue *q);

#endif //ASS1_QUEUE_H

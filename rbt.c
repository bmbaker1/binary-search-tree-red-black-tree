/* Brandon Baker
 * CS 201
 * Assignment #2
 * rbt.c
 */

#include "rbt.h"
#include <stdlib.h>
#include <stdarg.h>

#define RED 'R'
#define BLACK 'B'


/* Private Variables */

typedef struct rbtvalue {
	void *key;
	int freq, color;
	void (*display)(FILE *, void *);
	int (*compare)(void *, void *);
} rbtvalue;


/* Private Method Prototypes */

static void fatalRBT(char *message, ...);
static void nonFatalRBT(char *message, ...);

static rbtvalue *newRBTValue(void *value, void (*display)(FILE *, void *),
											int (*compare)(void *, void *));
static int compareRBTValue(void *this, void *that);
static void displayNotFoundErrorRBT(rbt *rbtree, void *value);
static void displayRBTValue(FILE *fp, void *value);
static int isLeftChild(bstNode *node);

static bstNode *getGramps(bstNode *node);
static bstNode *getParent(bstNode *node);
static bstNode *getLeftChild(bstNode *node);
static bstNode *getRightChild(bstNode *node);
static bstNode *getUncle(bstNode *node);
//static bstNode *getSibling(bstNode *node);
//static bstNode *getNiece(bstNode *node);
//static bstNode *getNephew(bstNode *node);

static int getColor(bstNode *node);
static void colorBlack(bstNode *node);
static void colorRed(bstNode *node);
//static void matchParentColor(bstNode *node);

static void leftRotate(bst *tree, bstNode *x);
static void rightRotate(bst *tree, bstNode *x);
static void insertionFixup(bst *tree, bstNode *node);
//static void deletionFixup(bst *tree, bstNode *node);

/* Private Method Implementations */

void fatalRBT(char *message, ...) {
	va_list ap;

	fprintf(stderr, "Error: ");
	va_start(ap, message);
	vfprintf(stderr, message, ap);
	vfprintf(stderr, "... Exiting\n", ap);
	va_end(ap);

	exit(666);
}

void nonFatalRBT(char *message, ...) {
	va_list ap;

	va_start(ap, message);
	vfprintf(stderr, message, ap);
	va_end(ap);
}

rbtvalue *newRBTValue(void *value, void (*display)(FILE *, void *),
									int (*compare)(void *, void *)) {
	rbtvalue *newrbtval;
	if((newrbtval = malloc(sizeof(rbtvalue))) == 0)
		fatalRBT("RBTValue not allocated");

	newrbtval->key = value;
	newrbtval->freq = 1;
	newrbtval->color = BLACK;
	newrbtval->display = display;
	newrbtval->compare = compare;

	return newrbtval;
}

int compareRBTValue(void *this, void *that) {
	rbtvalue *rbtvalthis = this;
	rbtvalue *rbtvalthat = that;

	return rbtvalthat->compare(rbtvalthis->key, rbtvalthat->key);
}


void displayNotFoundErrorRBT(rbt *rbtree, void *value) {
	nonFatalRBT("Value ");
	rbtree->display(stderr, value);
	nonFatalRBT(" not found.\n");
}

void displayRBTValue(FILE *fp, void *value) {
	rbtvalue *rbtval = value;

	rbtval->display(fp, rbtval->key);

	if(rbtval->freq > 1)
		fprintf(fp, "-%d", rbtval->freq);

	fprintf(fp, "-%c", rbtval->color);
}

int isLeftChild(bstNode *node) {
	return(node->parent->left == node);
}

int isLinear(bstNode *node, bstNode *parent) {
	return ((isLeftChild(node) && isLeftChild(parent)) ||
			(!isLeftChild(node) && !isLeftChild(parent)));
}

bstNode *getGramps(bstNode *node) {
	return node->parent->parent;
}

bstNode *getParent(bstNode *node) {
	return node->parent;
}

bstNode *getLeftChild(bstNode *node) {
	return node->left;
}

bstNode *getRightChild(bstNode *node) {
	return node->right;
}

bstNode *getUncle(bstNode *node) {
	if(isLeftChild(getParent(node))) return getRightChild(getGramps(node));
	else return getLeftChild(getGramps(node));
}

//bstNode *getSibling(bstNode *node) {
//	if(isLeftChild(node))	return getRightChild(node->parent);
//	else return getLeftChild(node->parent);
//}

//bstNode *getNiece(bstNode *node) {
//	if(isLeftChild(node)) return getLeftChild(getSibling(node));
//	else return getRightChild(getSibling(node));
//}

//bstNode *getNephew(bstNode *node) {
//	if(isLeftChild(node)) return getRightChild(getSibling(node));
//	else return getLeftChild(getSibling(node));
//}

int getColor(bstNode  *node) {
	if(node == NULL) return BLACK;

	rbtvalue *rbtval = node->value;

	return (rbtval->color == BLACK) ? BLACK : RED;
}

void colorBlack(bstNode *node) {
	rbtvalue *rbtval = 	node->value;
	rbtval->color = BLACK;
}

void colorRed(bstNode *node) {
	rbtvalue *rbtval = 	node->value;
	rbtval->color = RED;
}

//void matchParentColor(bstNode *node) {
//	if(getColor(node->parent) == RED) colorRed(node);
//	else colorBlack(node);
//}

void leftRotate(bst *tree, bstNode *node) {
	bstNode *parent = getParent(node), *gramps = getGramps(node),
			*left = getLeftChild(node);
	node->left = parent;
	parent->parent = node;
	parent->right = left;

	if(left) left->parent = parent;

	if(parent == tree->root) {
		node->parent = node;
		tree->root = node;
	} else {
		if(gramps->left == parent) gramps->left = node;
		else gramps->right = node;

		node->parent = gramps;
	}
}

void rightRotate(bst *tree, bstNode *node) {
	bstNode *parent = getParent(node), *gramps = getGramps(node),
			*right = getRightChild(node);
	node->right = parent;
	parent->parent = node;
	parent->left = right;

	if(right) right->parent = parent;

	if(parent == tree->root) {
		node->parent = node;
		tree->root = node;
	} else {
		if(gramps->right == parent) gramps->right = node;
		else gramps->left = node;

		node->parent = gramps;
	}
}


void rotate(bst *tree, bstNode *node) {
	if(isLeftChild(node)) rightRotate(tree, node);
	else leftRotate(tree, node);
}

void insertionFixup(bst *tree, bstNode *node) {
	bstNode *oldparent;

	while(1) {
		if(tree->root == node) break;
		if(getColor(node->parent) == BLACK) break;
		if(getColor(getUncle(node)) == RED){
			colorBlack(node->parent);
			colorBlack(getUncle(node));
			colorRed(getGramps(node));
			node = getGramps(node);
		} else {
			if(!isLinear(node, node->parent)) {
				oldparent = node->parent;
				rotate(tree, node);
				node = oldparent;
			}
			colorBlack(node->parent);
			colorRed(getGramps(node));
			rotate(tree, node->parent);
			break;
		}
	}
	colorBlack(tree->root);
}

//void deletionFixup(bst *tree, bstNode *node) {
//	while(1) {
//		if(node == tree->root) break;
//		if(getColor(node) == RED) break;
//		if(getColor(getSibling(node)) == RED) {
//			colorRed(node->parent);
//			colorBlack(getSibling(node));
//			rotate(tree, getSibling(node));
//		} else if(getColor(getNephew(node)) == RED) {
//			matchParentColor(getSibling(node));
//			colorBlack(getParent(node));
//			colorBlack(getNephew(node));
//			rotate(tree, getSibling(node));
//			break;
//		} else if(getColor(getNiece(node)) == RED) {
//			colorBlack(getNiece(node));
//			colorRed(getSibling(node));
//			rotate(tree, getNiece(node));
//		} else {
//			colorRed(getSibling(node));
//			node = node->parent;
//		}
//	}
//	colorBlack(node);
//}

/* Public Interface Implementations */

rbt *newRBT(void (*display)(FILE *, void *),
				   int (*comparator)(void *, void *)) {
	rbt *newrbt;
	if((newrbt = malloc(sizeof(rbt))) == 0)
		fatalRBT("RBT not allocated");

	newrbt->tree = newBST(displayRBTValue, compareRBTValue);
	newrbt->size = newrbt->words = 0;
	newrbt->comparator = comparator;
	newrbt->display = display;

	return newrbt;
}

void insertRBT(rbt *rbt, void *value) {
	rbtvalue *rbtval = newRBTValue(value, rbt->display, rbt->comparator);
	bstNode *existing = findBSTNode(rbt->tree, rbtval),
			*node;

	if(existing) {
		rbtval = existing->value;
		rbtval->freq++;
	} else {
		rbtval->color = RED;
		node = insertBST(rbt->tree, rbtval);

		insertionFixup(rbt->tree, node);

		rbt->size++;
	}

	rbt->words++;
}

int findRBT(rbt *rbt, void *value) {
	rbtvalue *rbtvalue = newRBTValue(value, rbt->display, rbt->comparator);
	bstNode *existing = findBSTNode(rbt->tree, rbtvalue);

	if(!existing) return 0;
	else rbtvalue = existing->value;

	return rbtvalue->freq;
}

void deleteRBT(rbt *rbt, void *value) {
	rbtvalue *rbtval = newRBTValue(value, rbt->display, rbt->comparator);
	bstNode *nix = findBSTNode(rbt->tree, rbtval);

	if(!nix) displayNotFoundErrorRBT(rbt, value);
	else {
		rbtval = nix->value;

		if(rbtval->freq > 1) rbtval->freq--;
		else {
			nix = swapToLeafBSTNode(nix);
//			deletionFixup(rbt->tree, nix);
			pruneBSTNode(rbt->tree, nix);
		}
		rbt->words--;
	}
}

int sizeRBT(rbt *rbt) {
	return rbt->tree->size;
}

int wordsRBT(rbt *rbt) {
	return rbt->words;
}

void statisticsRBT(rbt *rbt, FILE *fp) {
	fprintf(fp, "Words/Phrases: %d\n", wordsRBT(rbt));
	statisticsBST(rbt->tree, fp);
}

void displayRBT(FILE *fp, rbt *rbt) {
	displayBST(fp, rbt->tree);
}

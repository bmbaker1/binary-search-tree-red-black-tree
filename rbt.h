/* Brandon Baker
 * CS 201
 * Assignment #2
 * rbt.h
 * Creates and maintains a Red-Black Binary Search Tree by using the public
 * interface of the bst module
 */

#ifndef ASS2_RBT_H
#define ASS2_RBT_H

#include <stdio.h>
#include "bst.h"

typedef struct rbt {
	bst *tree;
	int size, words;
	void (*display)(FILE *, void *);
	int (*comparator)(void *, void *);
} rbt;


/*
 * function:	newRBT
 * parameter/s:	display and comparator functions for generic data type
 * description: creates a new rbt object
 * returns: 	the created rbt object
 */
extern rbt *newRBT(void (*display)(FILE *, void *),
				     				  int (*comparator)(void *, void *));

/*
 * function:	insertRBT
 * parameter/s:	red-black tree, value to be inserted
 * description: inserts the given value into the red-black tree
 * returns: 	void
 */
extern void insertRBT(rbt *rbt, void *value);

/*
 * function:	findRBT
 * parameter/s:	red-black tree, value to search for
 * description: finds the value given in the given red-black tree
 * returns: 	the frequency count of the found value in the given
 * 				red-black tree, 0 if it does not exist
 */
extern int findRBT(rbt *rbt, void *value);

/*
 * function:	deleteRBT
 * parameter/s:	red-black tree, value to be deleted
 * description: deletes a node, if it exists, in a red-black tree
 * returns: 	void
 */
extern void deleteRBT(rbt *rbt, void *value);

/*
 * function:	sizeRBT
 * parameter/s:	red-black tree
 * description: gets the amount of nodes in the give red-black tree
 * returns: 	the size of the red-black tree
 */
extern int sizeRBT(rbt *rbt);

/*
 * function:	wordsRBT
 * parameter/s:	red-black tree
 * description: gets the word count of the given red-black tree
 * returns: 	the amount of words in the red-black tree
 */
extern int wordsRBT(rbt *rbt);

/*
 * function:	statisticsRBT
 * parameter/s:	red-black tree, output file
 * description: prints the statistics of the give red-black tree
 * returns: 	void
 */
extern void statisticsRBT(rbt *rbt, FILE *fp);

/*
 * function:	displayRBT
 * parameter/s:	output file, red-black tree to display
 * description: displays the given red-black tree in level order
 * returns: 	void
 */
extern void displayRBT(FILE *fp, rbt *rbt);

#endif //ASS2_RBT_H

/* Brandon Baker
 * CS 201
 * Assignment #1
 * real.c
 */

#include "real.h"
#include <stdlib.h>
#include "utils.h"


/* Public Interface Implementations */

real *newReal(double v) {
	real *newreal;
	if((newreal = malloc(sizeof(real))) == 0)
		fatal("Real number not allocated... Exiting\n");

	newreal->value = v;

	return newreal;
}

double getReal(real *this) {
	return this->value;
}

void displayReal(FILE *out, void *value) {
	fprintf(out, "%f", getReal(value));
}


/* Brandon Baker
 * CS 201
 * Assignment #1
 * real.h
 * [description]
 */
#ifndef ASS1_REAL_H
#define ASS1_REAL_H

#include <stdio.h>

typedef struct real {
	double value;
} real;

extern real *newReal(double);
extern double getReal(real *);
extern void displayReal(FILE *, void *);

#endif //ASS1_REAL_H

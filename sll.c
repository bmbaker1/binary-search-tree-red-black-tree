/*
 * Brandon Baker
 * CS 201
 * Assignment #1
 * sll.c
 */

#include "sll.h"
#include <stdlib.h>
#include <stdarg.h>

/* Private Methods */

void fatalSLL(char *message, ...) {
	va_list ap;

	fprintf(stderr, "Error: ");
	va_start(ap, message);
	vfprintf(stderr, message, ap);
	va_end(ap);

	exit(666);
}

sllnode *createNewSLLNode(void *value) {
	sllnode *newnode;
	if((newnode = malloc(sizeof(sllnode))) == 0)
		fatalSLL("SLL node not allocated... Exiting\n");

	newnode->value = value;
	newnode->next = NULL;

	return newnode;
}

int isEmptySLL(sll *list) {
	return (list->head == NULL) ? 1 : 0;
}

sllnode *getNodeSLL(sll *list, int index) {
	if(index == 0) return list->head;
	if(index == list->size - 1) return list->tail;
	else {
		sllnode *current = list->head;
		int i = 0;

		while(i != index) {
			current = current->next;
			i++;
		}
		return current;
	}
}

void insertHeadSLL(sll *list, sllnode *newnode) {
	sllnode *head = list->head;

	list->head = newnode;
	list->head->next = head;
}

void insertTailSLL(sll *list, sllnode *newnode) {
	sllnode *tail = list->tail;

	tail->next = newnode;
	list->tail = newnode;
}

void insertMiddleSLL(sll *list, sllnode *newnode, int index) {
	sllnode *middle = getNodeSLL(list, index - 1);

	newnode->next = middle->next;
	middle->next = newnode;
}

sllnode  *removeHeadSLL(sll *list) {
	sllnode *head = list->head;

	if(list->head->next != NULL) list->head = list->head->next;
	else list->head = NULL;

	return head;
}

sllnode *removeTailSLL(sll *list) {
	sllnode *tail = list->tail, *previous;

	if(list->size > 1) {
		previous = getNodeSLL(list, list->size - 2);
		list->tail = previous;
	}
	list->tail->next = NULL;

	return tail;
}

sllnode *removeMiddleSLL(sll *list, int index) {
	sllnode *previous = getNodeSLL(list, index - 1),
			 *middle = previous->next;

	previous->next = middle->next;
	middle->next = NULL;

	return middle;
}


/* Public Interface Implementations */

sll *newSLL(void (*d)(FILE *, void *)) {
	sll *newsll;
	if((newsll = malloc(sizeof(sll))) == 0)
		fatalSLL("SLL not allocated... Exiting\n");

	newsll->head = newsll->tail = NULL;
	newsll->size = 0;
	newsll->display = d;

	return newsll;
}

void insertSLL(sll *list, int index, void *value) {
	sllnode *newnode = createNewSLLNode(value);

	if(isEmptySLL(list)) {
		list->head = list->tail = newnode;
	}
	else {
		if(index == 0) insertHeadSLL(list, newnode);
		else if(index == list->size) insertTailSLL(list, newnode);
		else insertMiddleSLL(list, newnode, index);
	}
	list->size++;
}

void *removeSLL(sll *list, int index) {
	sllnode *current;
	if(isEmptySLL(list)) fatalSLL("Removing from empty list... Exiting\n");

	if(index == 0) current = removeHeadSLL(list);
	else if(index == list->size - 1) current = removeTailSLL(list);
	else current = removeMiddleSLL(list, index);

	list->size--;
	return current->value;
}

void unionSLL(sll *recip, sll *donor) {
	if(isEmptySLL(donor)) return;
	if(isEmptySLL(recip)) {
		recip->head = donor->head;
	} else {
		recip->tail->next = donor->head;
		recip->tail = donor->tail;
	}
	recip->size += donor->size;
	donor->head = NULL;
	donor->size = 0;
}

void *getSLL(sll *list, int index) {
	return getNodeSLL(list, index)->value;
}

int sizeSLL(sll *s_list) {
	return s_list->size;
}

void displaySLL(FILE *file, sll *list) {
	sllnode *current = list->head;

	fprintf(file, "[");
	while(current != NULL) {
		list->display(file, current->value);
		current = current->next;

		if(current != NULL) fprintf(file, ",");
	}
	fprintf(file, "]");
}

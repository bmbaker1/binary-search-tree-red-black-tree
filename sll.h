/*
 * Brandon Baker
 * CS 201
 * Assignment #1
 * sll.h
 * User interface for a module that creates and maintains a singly-linked list
 */

#ifndef ASS1_SLL_H
#define ASS1_SLL_H

#include <stdio.h>

typedef struct sllnode {
	void *value;
	struct sllnode *next;
} sllnode;

typedef struct sll {
	sllnode *head, *tail;
	int size;
	void (*display)(FILE *, void *);
} sll;

/*
 * function:    newSLL
 * parameter/s: displayer function for generic data type
 * description: allocates a new singly-linked list
 * returns:     an empty singly-linked list
 */
extern sll *newSLL(void (*d)(FILE *, void *));

/*
 * function:    insertSLL
 * parameter/s: singly-linked list, index, generic value
 * description: inserts generic value into given singly-linked list at specified
 *              index
 * returns:     void
 */
extern void insertSLL(sll *list, int index, void *value);

/*
 * function:    removeSLL
 * parameter/s: singly-linked list, index
 * description: removes element from singly-linked list at specified index
 * returns:     generec value of removed element
 */
extern void *removeSLL(sll *list, int index);

/*
 * function:    unionSLL
 * parameter/s: singly-linked list, singly-linked list
 * description: adds singly-linked list to the back of recipient singly-linked
 *              list.  Empties donor list.
 * returns:     void
 */
extern void unionSLL(sll *recip, sll *donor);

/*
 * function     getSLL
 * parameter/s: singly-linked list, index
 * description: finds element at specified index and returns the value without
 * 				deleting the element
 * 	returns:    generic value at specified index
 */
extern void *getSLL(sll *list, int index);

/*
 * function:    sizeSLL
 * parameter/s: singly-linked list
 * description: returns the size of the specified singly-linked list
 * returns :    size of specified singly-linked list
 */
extern int sizeSLL(sll *list);

/*
 * function:    displaySLL
 * parameter/s: output file, singly-linked list
 * description: displays singly-linked list in form of [n0, n1, n2, ..., nk]
 * 				to the specified output location
 * returns:     void
 */
extern void displaySLL(FILE *file, sll *list);

#endif //ASS1_SLL_H

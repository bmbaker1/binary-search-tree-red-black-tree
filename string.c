/* Brandon Baker
 * CS 201
 * Assignment #2
 * string.c
 */

#include "string.h"
#include <stdlib.h>
#include <string.h>
#include "utils.h"


/* Public Interface Implementations */

string *newString(char *v) {
	string *newstring;
	if((newstring = malloc(sizeof(string))) == 0)
		fatal("String not allocated... Exiting\n");

	newstring->value = v;

	return newstring;
}

char *getString(string *this) {
	return this->value;
}

void displayString(FILE *out, void *value) {
	fprintf(out, "\"%s\"", getString(value));
}

int compareString(void *this, void *that) {
	return (strcmp(((string*)this)->value, ((string*)that)->value));
}



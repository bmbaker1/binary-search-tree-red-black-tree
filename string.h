/* Brandon Baker
 * CS 201
 * Assignment #2
 * string.h
 * [description]
 */

#ifndef ASS2_STRING_H
#define ASS2_STRING_H

#include <stdio.h>

typedef void (*displayer)(FILE *, void *);
typedef int (*comparator)(void *, void *);

typedef struct string {
	char *value;
} string;

extern string *newString(char *);
extern char *getString(string *);
extern void displayString(FILE *, void *);
extern int compareString(void *, void *);

#endif //ASS2_STRING_H

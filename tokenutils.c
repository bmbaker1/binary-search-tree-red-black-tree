/* Brandon Baker
 * CS 201
 * Assignment #1
 * tokenutils.c
 */

#include "tokenutils.h"
#include <ctype.h>
#include <string.h>
#include "scanner.h"

void *getNextToken(FILE *input) {
	char *token;

	token = (stringPending(input)) ? readString(input) : readToken(input);

	if(token != NULL) cleanToken(token);

	return token;
}

char getNextCommand(FILE *input) {
	return readChar(input);
}


void cleanToken(char *token) {
	char *i = token, *j = token;

	do{
		if(isspace(*i)){
			while(isspace(*i) || ispunct(*i) || isdigit(*i))
				i++;
			*j++ = ' ';
		}
		else if(!isalpha(*i)) i++;
		else if(isupper(*i)) {
			*j++ = (char)tolower(*i);
			i++;
		}
		else if(i == j) {
			j++;
			i++;
		}
		else *j++ = *i++;
	}while(*i);

	*j = 0;
}

int isEmptyToken(char *token) {
	return (strcmp(token, "") == 0);
}

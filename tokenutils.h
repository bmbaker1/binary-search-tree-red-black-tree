/* Brandon Baker
 * CS 201
 * Assignment #1
 * tokenutils.h
 * Public interface for tokenutils module, which is used to get and clean
 * tokens from an input file.
 */

#ifndef ASS1_TOKENUTILS_H
#define ASS1_TOKENUTILS_H

#include <stdio.h>

/*
 * function:	getNextToken
 * parameter/s:	input file
 * description: gets the next token from the given input file, whether just a
 * 				token or a string
 * returns:		the read in token
 */
extern void *getNextToken(FILE *input);

/*
 * function:	getNextCommand
 * parameter/s:	command file to read from
 * description:	reads in the next character in a given command file.  Will
 * 				either be i, d, f, s, or r
 * returns:		the read in character
 */
extern char getNextCommand(FILE *input);

/*
 * function:	cleanToken
 * parameter/s:	token
 * description:	removes all digits and symbols from the given token.  lowers
 * 				all alpha characters.  condenses all spans of whitespace to a
 * 				single space
 * returns:		void
 */
extern void cleanToken(char *token);

/*
 * function:	isEmptyToke
 * parameter/s:	token
 * description:	checks if given token is empty or not
 * returns:		1 if token != "", 0 otherwise
 */
extern int isEmptyToken(char *token);

#endif //ASS1_TOKENUTILS_H

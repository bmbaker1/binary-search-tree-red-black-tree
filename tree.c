/* Brandon Baker
 * CS 201
 * Assignment #2
 * options.c
 */

#include "tree.h"
#include "vbst.h"
#include "rbt.h"
#include "string.h"
#include "tokenutils.h"

#define VBST 'v'


/* Public Interface Implementations */

void *plantTree(char treetype) {
	if(treetype == VBST) return newVBST(displayString, compareString);
	else return newRBT(displayString, compareString);
}

void growTree(char treetype, void *tree, FILE *corpus) {
	void *token = getNextToken(corpus);

	while(!feof(corpus)) {
		if(!isEmptyToken(token)) {
			if(treetype == VBST) insertVBST(tree, newString(token));
			else insertRBT(tree, newString(token));
		}

		token = getNextToken(corpus);
	}
}

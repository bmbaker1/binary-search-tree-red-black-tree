/* Brandon Baker
 * CS 201
 * Assignment #2
 * options.h
 * [description]
 */
#ifndef ASS2_OPTIONS_H
#define ASS2_OPTIONS_H

#include <stdio.h>


/*
 * function: 	plantTree
 * parameter/s: type of tree to plant
 * description: creates a new tree object based on tree type given
 * returns:		either an empty rbt or vbst object
 */
extern void *plantTree(char treetype);

/*
 * function:	growTree
 * parameter/s: type of tree, generic tree pointer, input file
 * description: reads all phrases in the given input and inserts it into
 * 				the give tree
 * returns:		void
 */
extern void growTree(char treetype, void *tree, FILE *corpus);

#endif //ASS2_OPTIONS_H

/* Brandon Baker
 * CS 201
 * Assignment #1
 * utils.c
 */

#include "utils.h"
#include <stdlib.h>
#include <stdarg.h>

/* Private Method Headers */

static int validFlag(char flag);


/* Private Method Implementations */

int validFlag(char flag) {
	return flag == 'v' || flag == 'r';
}


/* Public Method Implementations */

void fatal(char *message, ...) {
	va_list ap;

	fprintf(stderr, "Error: ");
	va_start(ap, message);
	vfprintf(stderr, message, ap);
	vfprintf(stderr, "... Exiting\n", ap);
	va_end(ap);

	exit(666);
}

void nonFatal(char *message, ...) {
	va_list ap;

	va_start(ap, message);
	vfprintf(stderr, message, ap);
	va_end(ap);
}

char getFlag(char **argv) {
	if(argv[1])	{
		char flag = argv[1][1];
		if(!validFlag(flag)) fatal("Unknown tree type");

		return flag;
	}

	fatal("Please run with the proper arguments");
	return 0;
}

FILE *getCorpus(char **argv) {
	if(argv[2]) {
		FILE *corpus = fopen(argv[2], "r");
		return corpus;
	}

	fatal("Please run with the proper arguments");
	return 0;
}

FILE *getCommandFile(char **argv) {
	if(argv[3]) {
		FILE *commands = fopen(argv[3], "r");
		return commands;
	}

	fatal("Please run with the proper arguments");
	return 0;
}

FILE *getOutputFile(char **argv) {
	if(argv[4]) return fopen(argv[4], "w");
	else return stdout;
}

void newline() {
	printf("\n");
}

void hereone() {
	printf("11111     HERE     11111\n");
}

void heretwo() {
	printf("22222     HERE     22222\n");
}

void herethree() {
	printf("33333     HERE     33333\n");
}

/*
 * Brandon Baker
 * CS 201
 * Assignment #1
 * utils.h
 * Utility module that holds a fatal and debugging functions
 */

#ifndef ASS1_UTILS_H
#define ASS1_UTILS_H

#include <stdio.h>

/* Error Handling Functions */
void fatal(char *, ...);
void nonFatal(char *, ...);

/* Project Specific Functions */
char getFlag(char **argv);
FILE *getCorpus(char **argv);
FILE *getCommandFile(char **argv);
FILE *getOutputFile(char **argv);

/* Print functions for debugging */
void newline(void);
void hereone(void);
void heretwo(void);
void herethree(void);

#endif //ASS1_UTILS_H

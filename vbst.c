/* Brandon Baker
 * CS 201
 * Assignment #2
 * vbst.c
 */

#include "vbst.h"
#include <stdlib.h>
#include <stdarg.h>


/* Private Variables */

typedef struct vbstvalue {
	void *key; //string value
	int freq;
	void (*display)(FILE *, void *);
	int (*compare)(void *, void *);
} vbstvalue;


/* Private Method Prototypes */
static void fatalVBST(char *message, ...);
static void nonFatalVBST(char *message, ...);

static vbstvalue *newVBSTValue(void *value, void (*display)(FILE *, void *),
							   				int (*compare)(void *, void *));
static int compareVBSTValue(void *this, void *that);
static void displayNotFoundErrorVBST(vbst *vbstree, void *value);
static void displayVBSTValue(FILE *fp, void *value);


/* Private Method Implementations */

void fatalVBST(char *message, ...) {
	va_list ap;

	fprintf(stderr, "Error: ");
	va_start(ap, message);
	vfprintf(stderr, message, ap);
	vfprintf(stderr, "... Exiting\n", ap);
	va_end(ap);

	exit(666);
}

void nonFatalVBST(char *message, ...) {
	va_list ap;

	va_start(ap, message);
	vfprintf(stderr, message, ap);
	va_end(ap);
}

vbstvalue *newVBSTValue(void *value, void (*display)(FILE *, void *),
						             int(*compare)(void *, void *)){
	vbstvalue *newvbstvalue;
	if((newvbstvalue = malloc(sizeof(vbstvalue))) == 0)
		fatalVBST("VBSTValue not allocated");

	newvbstvalue->key = value;
	newvbstvalue->freq = 1;
	newvbstvalue->display = display;
	newvbstvalue->compare = compare;

	return newvbstvalue;
}

int compareVBSTValue(void *this, void *that) {
	vbstvalue *vbstvalthis = this;
	vbstvalue *vbstvalthat = that;

	return vbstvalthat->compare(vbstvalthis->key, vbstvalthat->key);
}

void displayNotFoundErrorVBST(vbst *vbstree, void *value) {
	nonFatalVBST("Value ");
	vbstree->display(stderr, value);
	nonFatalVBST(" not found.\n");
}

void displayVBSTValue(FILE *fp, void *value) {
	vbstvalue *vbstval = value;

	vbstval->display(fp, vbstval->key);

	if(vbstval->freq > 1)
		fprintf(fp, "-%d", vbstval->freq);
}


/* Public Interface Implementations */

vbst *newVBST(void (*display)(FILE *, void *),
					 int (*comparator)(void *, void *)) {
	vbst *newvbst;
	if((newvbst = malloc(sizeof(vbst))) == 0)
		fatalVBST("VBST not allocated");

	newvbst->tree = newBST(displayVBSTValue, compareVBSTValue);
	newvbst->size = newvbst->words = 0;
	newvbst->comparator = comparator;
	newvbst->display = display;

	return newvbst;
}

void insertVBST(vbst *vbst, void *value) { //value is a string object
	vbstvalue *vbstval = newVBSTValue(value, vbst->display, vbst->comparator);
	bstNode *existing = findBSTNode(vbst->tree, vbstval);

	if(existing) { // if the value was found...
		vbstval = existing->value;
		vbstval->freq++;
	} else {
		insertBST(vbst->tree, vbstval);
		vbst->size++;
	}
	vbst->words++;
}

int findVBST(vbst *vbst, void *value) {
	vbstvalue *vbstval = newVBSTValue(value, vbst->display, vbst->comparator);
	bstNode *existing = findBSTNode(vbst->tree, vbstval);

	if(!existing) return 0;

	vbstval = existing->value;

	return vbstval->freq;
}

void deleteVBST(vbst *vbst, void *value) {
	vbstvalue *vbstval = newVBSTValue(value, vbst->display, vbst->comparator);
	bstNode *nix = findBSTNode(vbst->tree, vbstval);

	if(!nix) displayNotFoundErrorVBST(vbst, value);
	else {
		vbstval = nix->value;

		if(vbstval->freq > 1) vbstval->freq--;
		else {
			nix = swapToLeafBSTNode(nix);
			pruneBSTNode(vbst->tree, nix);
		}
		vbst->words--;
	}
}

int sizeVBST(vbst *vbst) {
	return sizeBST(vbst->tree);
}

int wordsVBST(vbst *vbst) {
	return vbst->words;
}

void statisticsVBST(vbst *vbst, FILE *fp) {
	fprintf(fp, "Words/Phrases: %d\n", wordsVBST(vbst));
	statisticsBST(vbst->tree, fp);
}

void displayVBST(FILE *fp, vbst *vbst) {
	displayBST(fp, vbst->tree);
}

/* Brandon Baker
 * CS 201
 * Assignment #2
 * vbst.h
 * Creates and maintains a Vanilla Binary Search Tree by using the public
 * inerface of the bst module
 */

#ifndef ASS2_VBST_H
#define ASS2_VBST_H

#include <stdio.h>
#include "bst.h"

typedef struct vbst {
	bst *tree;
	int size, words; // words = amount of unique words
	void (*display)(FILE *, void *);
	int (*comparator)(void *, void *);
} vbst;


/*
 * function: 	newVBST
 * parameter/s:	display and comparator functions for generic data type
 * description:	creates a new vbst object
 * returns:		the created vbst object
 */
extern vbst *newVBST(void (*display)(FILE *, void *),
					        int (*comparator)(void *, void *));

/*
 * function:	insertVBST
 * parameter/s:	vanilla bst, value to be inserted
 * description:	inserts the given value into the vanilla bst
 * returns:		void
 */
extern void insertVBST(vbst *vbst, void *value);

/*
 * function:	findVBST
 * parameter/s:	vanilla bst, value to search for
 * description:	finds the value given in the given vanilla bst
 * returns:		the frequency count of the found value in the given
 * 				vanilla bst, 0 if it does not exist
 */
extern int findVBST(vbst *vbst, void *value);

/*
 * function:	deleteVBST
 * parameter/s:	vanilla bst, value to be deleted
 * description:	deletes a node, if it exists, in a vanilla bst
 * returns:		void
 */
extern void deleteVBST(vbst *vbst, void *vlaue);

/*
 * function:	sizeVBST
 * parameter/s:	vanilla bst
 * description:	gets the amount of nodes in the given vanilla bst
 * returns:		the size fot he vanilla bst
 */
extern int sizeVBST(vbst *vbst);

/*
 * function:	wordsVBST
 * parameter/s:	vanilla bst
 * description:	get the word count of the give vanilla bst
 * returns:		the amount of words in the vanilla bst
 */
extern int wordsVBST(vbst *vbst);

/*
 * function:	statisticsVBST
 * parameter/s:	vanilla bst, output file
 * description:	prints the statistics of the given vanilla bst
 * returns:		void
 */
extern void statisticsVBST(vbst *vbst, FILE *fp);

/*
 * function:	displayVBST
 * parameter/s:	output fule, vanilla bst to display
 * description:	displays the given vanila bst in level order
 * returns:		void
 */
extern void displayVBST(FILE *fp, vbst *vbst);

#endif //ASS2_VBST_H
